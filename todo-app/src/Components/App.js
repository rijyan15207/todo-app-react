import React, { Component } from 'react'; 
import AddNewTask from './AddNewTask/AddNewTask';
import ToDoAppList from './ToDoAppList/ToDoAppList';
import './App.css'; 

class App extends Component { 

  constructor(props) { 
    super(props); 
    this.state = { 
      tasks: [] 
    }; 
  } 

  updateList(text) { 
    let updatedTask = this.state.tasks; 
    updatedTask.push(text); 
    this.setState({ 
      tasks: updatedTask 
    }) 
  } 

  deleteTask(index) { 
    let allTasks = this.state.tasks;
    allTasks.splice(index, 1);

    this.setState ({ 
      tasks: allTasks 
    }); 
  } 
  
  


  render() { 
    return ( 
      <div className="App container mt-5"> 
        <div className="row"> 
          <div className="col-sm-6 offset-sm-3"> 
            <div className="card mt-5"> 
              
              <div className="card-header text-center display-4 bg-info text-white"> 
                ToDo App 
              </div> 

              <div className="card-body"> 
                <AddNewTask updateList={ this.updateList.bind(this) }/> 
                <ToDoAppList tasks={ this.state.tasks }
                  deleteTask={this.deleteTask.bind(this)}/> 
              </div> 
              
            </div> 
          </div> 
        </div> 
      </div> 
    ); 
  } 
} 

export default App; 

