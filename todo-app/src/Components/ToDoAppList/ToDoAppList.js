import React, {Component} from 'react';
import './ToDoAppList.css';

class ToDoAppList extends Component { 
    // constructor(props) { 
    //     super(props); 
    // } 


    

    render() { 
        let item = this.props.tasks.map((task, i) => { 
            return ( 
                <li key={i} className="mt-2 aaa"> 
                    <table class="table table-striped table-bordered table-responsive"> 
                        <tbody> 
                            <tr> 
                                <td scope="row" data-toggle="tooltip" data-placement="top" title="Edit"> 
                                    <i class="fa fa-edit" ></i> 
                                </td> 
                                <td>
                                <input type="submit" value="X" className="btn btn-sm    
                                    btn-danger mr-3" data-toggle="tooltip" data-placement="top" title="Delete"
                                    onClick={() => { 
                                        this.props.deleteTask(i) 
                                    }} 
                                /> 
                                </td> 
                                <td><span>{ task }</span> </td> 
                            </tr> 
                        </tbody> 
                    </table> 
                </li> 
            ) 
        }); 

        return ( 
            <div className="mt-4"> 
                <ul> 
                    { item } 
                </ul> 
            </div> 
        ) 
    } 
} 

export default ToDoAppList;



