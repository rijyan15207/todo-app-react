import React, {Component} from 'react';

class AddNewTask extends Component { 
    // constructor(props) { 
    //     super(props); 
    // } 


    submitted(event) {
        event.preventDefault(); 
        let text = event.target.querySelector('input'); 
        let task = text.value; 
        text.value = ''; 
        this.props.updateList(task); 
    } 

    render() { 
        return ( 
            <div> 
                <form onSubmit={this.submitted.bind(this)}> 
                    <input type="text" className="form-control" 
                           placeholder="Enter Your Task" required/> 
                    <button className="btn btn-sm btn-info mt-2">Submit</button> 
                </form> 
            </div> 
        ) 
    } 
} 

export default AddNewTask 