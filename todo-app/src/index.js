import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './Components/App';
import registerServiceWorker from './registerServiceWorker';

import './../node_modules/bootstrap/dist/css/bootstrap.min.css';

// import './../node_modules/jquery/dist/jquery.js';
// import './../node_modules/popper.js/dist/popper.js';
// import './../node_modules/bootstrap/dist/js/bootstrap.js';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
